#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

char	*ft_strrev(char *str)
{
	int i;
	int j;
	char tmp;

	i = 0;
	j = 0;
	while (str[i])
		i++;
	i--;
	while (j < i)
	{
		tmp = str[i];
		str[i] = str[j];
		str[j] = tmp;
		j++;
		i--;
	}
	return(str);
}

int		main(int argc, char **argv)
{
	(argc != 2) ? printf("veuillez entrez une string\n") : printf("%s\n", ft_strrev(argv[1]));
	return(0);
}
