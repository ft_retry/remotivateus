#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		putchar(str[i]);
		i++;
	}
}

int		main()
{
	char *str = "le lapin de paques du ghetto";
	ft_putstr(str);
	return(0);
}
