#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int		ft_atoi(char *str)
{
	int i;
	int nb;
	int neg;

	neg = 1;
	nb = 0;
	i = 0;
	while (str[i] == 'n' || str[i] == 't' || str[i] == 'v' || str[i] == 'f' || str[i] == 'r' || str[i] == ' ')
	{
		i++;
		if (str[i] == '\0')
			return (0);
	}
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			neg = -neg;
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		nb = nb * 10 + str[i] - '0';
		i++;
	}
	return (nb * neg);
}

int		main()
{
	char *str = "          ";
	printf("%d\n", ft_atoi(str));
	return(0);
}
