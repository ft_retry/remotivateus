#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	*ft_char_order(char	*str)
{
	int i;
	int j;
	char stock;

	i = 0;
	j = i + 1;
	while (str[j])
	{
		if (str[j] < str[i])
		{
			stock = str[j];
			str[j] = str[i];
			str[i] = stock;
			i = 0;
			j = i + 1;
		}
		else
		{
			i++;
			j++;
		}
	}
	str[j] = '\0';
	return (str);
}

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("erreur nombre d'argument");
	else
		printf("%s\n", ft_char_order(av[1]));
	return(0);
}
