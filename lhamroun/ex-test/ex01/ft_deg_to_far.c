#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	ft_deg_to_far(int temp)
{
	int Far;

	if (temp <= -273 || temp >= 1000)
		;
	else
	{
		Far = (1.8 * temp) + 32;
		printf("%d °C =  %d °F\n", temp, Far);
	}
}

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("erreur nombre d'argument(s)");
	else
		ft_deg_to_far(atoi(av[1]));
	return(0);
}
