#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_enigme(int broke_floor)
{
	int O1;
	int O2;
	int i;
	int cmp;

	O1 = 9;
	O2 = 1;
	i = 16;
	cmp = 0;
	while (O1 <= broke_floor)
	{
		O1 = O1 + i--;
		cmp++;
	}
	O2 = O1 - i;
	while (O2 < broke_floor)
	{
		O2++;
		cmp++;
	}
	return(cmp);
}

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("erreur nombre d'argument");
	else
		printf("%d\n", ft_enigme(atoi(av[1])));
	return(0);
}
