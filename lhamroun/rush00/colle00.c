void	ft_putchar(char c);

void	colle(int a, int b)
{
	int x;
	int y;

	y = 0;
	x = 0;
	while(x < a && y < b) // derniere case du tableau
	{
		while (y < b) // fin d'une ligne
		{
			if ((x == 0 || x == a - 1) && (y == 0 || y == b - 1))
				ft_putchar('o');
			else if ((y > 0 && y < b - 1) && (x == 0 || x == a - 1 ))
				ft_putchar('-');
			else if ((y == 0 || y == b - 1) && (x > 0 && x < a - 1))
				ft_putchar('|');
			else if (x > 0 && x < a - 1 && y > 0 && y < b - 1)
				ft_putchar(' ');
			y++;
		}
		ft_putchar('\n');
		y = 0;
		x++;

	}
}
