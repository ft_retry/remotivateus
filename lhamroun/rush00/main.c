#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	colle(int a, int b);

int		main(int ac, char **av) 
{
	int x;
	int y;

	if (ac != 3)
		return (printf("Veuillez entrer 2 paramètres\n"));
	else
	{
		y = atoi(av[1]);
		x = atoi(av[2]);
		colle(x, y);
		return (0);
	}
}
