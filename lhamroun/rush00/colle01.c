void	ft_putchar(char c);

void	colle(int a, int b)
{
	int x;
	int y;

	x = 0;
	y = 0;
	while (x < a && y < b)
	{
		while (y < b)
		{
			if ((y == 0 && x == 0) || (x == a - 1 && y == b - 1))
				ft_putchar('/');
			else if ((x == 0 && y == b - 1 ) || (x == a - 1 && y == 0))
				ft_putchar('\\');
			else if (y > 0 && y < b - 1 && x > 0 && x < b - 1)
				ft_putchar(' ');
			else
				ft_putchar('*');
			y++;
		}
		ft_putchar('\n');
		y = 0;
		x++;
	}
}
