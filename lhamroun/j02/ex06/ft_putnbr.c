#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		ft_nb_negative(int nb)
{
	ft_putchar('-');
	if (nb == -2147483648)
	{
		ft_putchar('2');
		nb = nb % 1000000000;	
	}
	nb = -nb;
	return (nb);
}

void	ft_putnbr(int nb)
{
	int x;
	int i;
	int tab[11];

	x = 0;
	i = 0;
	if (nb < 0)
		nb = ft_nb_negative(nb);
	if (nb < 10)
		ft_putchar(nb + '0');
	else
	{
		while (nb > 0)
		{
			tab[i] = nb % 10;
			nb = nb / 10;
			i++;
		}
		i--;
		while (i >= 0)
		{
			ft_putchar(tab[i] + '0');
			i--;
		}
	}
}
