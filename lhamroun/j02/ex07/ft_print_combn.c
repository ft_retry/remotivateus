#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	ft_putchar(char c);

void	ft_print_combn(int n)
{
	int i;
	int j;
	int x;
	int tab[9];

	i = 0;
	x = 0;
	if (n < 1 || n > 9)
		;
	else
	{
		while (i < n)
		{
			tab[i] = i;//on initilise le tab et ala fin i = n
			i++;
		}
		i--;
		while (tab[0] != 10 - n)
		{
			x = 1;
			while (tab[i] <= 9) //tant que dernier chiffre < 9
			{
				j = 0;
				while (tab[j])//putchar ts les chiffres
				{
					ft_putchar(tab[j] - '0');
					j++;
				}
				if (tab[0] != 10 - n)//si pas la fin, afficher ", "
				{
					ft_putchar(',');
					ft_putchar(' ');
				}
				tab[i]++; //dernier chiffre++
			}
			if (tab[i-x] == tab[i] - 1)
				x++;
			tab[i-x]++;
			while (x > 0)
			{
				tab[i-(x+1)] = tab[i-x]++;
				x--;
			}
			tab[i-x] = tab[i-x] + 1;
		}
	}
}
