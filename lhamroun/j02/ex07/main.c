#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_atoi(char *str);
void	ft_print_combn(int n);
void	ft_putchar(char c);

int		main(int ac, char **av)
{
	if (ac != 2)
		return (0);
	else
		ft_print_combn(ft_atoi(av[1]));
	return (0);
}
