#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_atoi(char *str)
{
	int i;
	int nbr;
	int neg;

	i = 0;
	nbr = 0;
	neg = 1;
	while (str[i] == ' ')
		i++;
	if (str[i] == '+')
		i++;
	else if (str[i] == '-')
	{
		neg = -1;
		i++;
	}
	if (str[i] > 47 && str[i] < 58)
	{
		while (str[i] > 48 && str[i] < 57)
		{
			nbr = nbr * 10 + (str[i] - '0');
			i++;
		}
	}
	return (nbr * neg);
}
