#include <unistd.h>

void	ft_print_reversealpha(void)
{
	char letr;

	letr = 'z';
	while (letr >= 'a')
	{
		write(1, &letr, 1);
		letr--;
	}
}
