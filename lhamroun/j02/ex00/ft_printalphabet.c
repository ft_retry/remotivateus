#include <unistd.h>

void	ft_printalphabet(void)
{
	char alpha;
	
	alpha = 'a';
	while (alpha <= 'z')
	{
		write(1, &alpha, 1); 
		alpha++;
	}
}
