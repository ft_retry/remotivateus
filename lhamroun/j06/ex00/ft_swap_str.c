#include <unistd.h>

int		*ft_swap_str(int *a, int *b)
{
	int *x;
	int i;

	i = 0;
	while (a[i] || b[i])
	{
		x[i] = a[i];
		a[i] = b[i];
		b[i] = x[i];
		i++;
	}
	return (a, b);
}

int		main(int ac, char **av)
{
	if (ac != 3)
		printf("erreur nombre d'argument");
	else
		printf("%d\n", ft_swap_str(atoi(av[1]), atoi(av[2])));
	return (0);
}
