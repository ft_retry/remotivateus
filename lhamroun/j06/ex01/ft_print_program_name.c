#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		main(int ac, char **av)
{
	int i;

	i = 0;
	if (ac != 1)
		printf("erreur nombre d'argument");
	else
	{
		while(av[0][i])
			ft_putchar(av[0][i++]);
	}
	return(0);
}
