#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		main(int ac, char** av)
{
	int i;
	int j;

	i = 1;
	if (ac < 2)
		printf("erreur nombre d'arguments");
	else
	{
		while(av[i])
			i++;
		i--;
		while(i >= 1)
		{
			j = 0;
			while(av[i][j])
				j++;
			while(j >= 0)
			{
				ft_putchar(av[i][j]);
				j--;
			}
			ft_putchar('\n');
			i--;
		}
	}
}
