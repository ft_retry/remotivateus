#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		ft_strcmp(char *arg1, char *arg2)
{
	int i;

	i = 0;
	while ((arg1[i] || arg2[i]) && arg1[i] == arg2[i])
		i++;
	return (arg1[i] - arg2[i]);
}

int		main(int ac, char **av)
{
	int i;
	int j;
	char **tab;
	char *swp;

	i = 1;
	if (ac < 2)
		printf("erreur nombre d'argument");
	else
	{
		while (av[i])
		{
			j = 0;
			while (av[i][j] != '\0')
			{
				tab[i - 1][j] = av[i][j]; //j'ai une erreur sur cette ligne mais je sais pas pourquoi
				j++;
			}
			tab[i - 1][j] = '\0';
			i++;
		}
		i = 0;
		while(tab[i] && tab[i + 1])
		{
			j = 0;
			if (ft_strcmp(tab[i], tab[i + 1]) < 0)
			{
				while (tab[i][j] || tab[i + 1][j])
				{
					j = 0;
					swp[j] = tab[i][j];
					tab[i][j] = tab[i + 1][j];
					tab[i + 1][j] = swp[j];
					j++;
				}
				i = 0;
			}
			else
				i++;
		}
	}
	return(0);
}
