#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	*ft_strdup(char *src)
{
	int i;
	int size_src;
	char* dest;

	i = 0;
	size_src = 0;
	//*dest = ""; //comment déclarer 1 tab
	while (src[size_src]) 
		size_src++;  // taille de la source
	dest = (char*)malloc(sizeof(char)*(size_src));
	while(src[i])
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("erreur nombre d'argument");
	else
		printf("%s\n", ft_strdup(av[1]));
	return(0);
}
