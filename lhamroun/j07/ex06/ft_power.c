#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_power(int nb, int power)
{
	if (power < 0)
		return (0);
	else if (power == 0)
		return (1);
	else if (power == 1)
		return (nb);
	else
	{
		nb = nb * ft_power(nb, --power);
		return (nb);
	}
}

//	int value;

//	value = 1;
//	while (power > 0)
//	{
//		value = value * nbr;
//		pow--;
//	}
//	return (value);

//int		main(int ac, char **av)
//{
//	printf("%d\n", ft_power(atoi(av[1]), atoi(av[2])));
//	return 0;
//}
