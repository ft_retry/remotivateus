#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_ultimate_range(int **range, int min, int max)
{
	int i;
	int *tab;

	i = 0;
	tab = (int*)malloc(sizeof(int)*(max - min));
	if (min >= max)
	{
		*range = NULL;
		return (0);
	}
	while (max - min > 0) //déja testé avec i<size et max>min
	{
		tab[i++]= min++;
		if (max - min == 0)
			return (i);
	}
	*range = tab;
	return (0);
}

int		main(int ac, char **av)
{
	int **range;

	if (ac != 3)
		printf("erreur nombre d'argument");
	else
		printf("%d\n", ft_ultimate_range(range, atoi(av[1]), atoi(av[2])));
	return(0);
}
