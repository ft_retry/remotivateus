#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	**ft_split_whitespaces(char *str)
{
	int i;
	int j;
	int x;
	char **tab;

	x = 0;
	i = 0;
	while (str[x])
		x++;
	tab = (char**)malloc(sizeof(char)*x);
	x = 0;
	while (str[x])
	{
		j = 0;
		while (str[x] < 33) //non imprimables
			x++;
		while (str[x] >= 33) //caracteres imprimables
		{
			tab[i][j++]=str[x++];
			tab[i][j]='\0';
		}
		tab[i] = (char*)malloc(sizeof(char)*j);
		while (str[x] < 33) //non imprimables
			x++;
		i++;
	}
	return (tab);
}

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("erreur nombre d'argument");
	else
		ft_split_whitespaces(av[1]);
	return(0);
}
