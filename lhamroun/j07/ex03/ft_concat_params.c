#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_count_char(char **argv)
{
	int i;
	int j;
	int cmp;

	i = 0;
	cmp = 0;
	while (argv[i])
	{
		j = 0;
		while (argv[i][j])
			j++;
		cmp=+j;
		i++;
	}
	return (cmp);
}

char	*ft_concat_params(int argc, char **argv)
{
	int i;
	int j;
	int k;
	char *str;

	i = 1;
	j = 0;
	k = 0;
	//str[argc] = ;
	(void)argc;
	str = (char*)malloc(sizeof(char)*ft_count_char(argv));
	while (argv[i])
	{
		j = 0;
		while (argv[i][j])
		{
			str[k] = argv[i][j];
			j++;
			k++;
		}
		str[k++] = '\n';
		i++;
	}
	str[k] = '\0';
	return (str);
}

int		main(int argc, char **argv)
{
	if (argc < 2)
		printf("erreur nombre d'argument");
	else
		printf("%s", ft_concat_params(argc, argv));
	return(0);
}
