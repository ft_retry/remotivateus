#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		*ft_range(int min, int max)
{
	int size;
	int *tab;
	int i;

	i = 0;
	if (min < max)
	{
		size = max - min;
		tab = (int*)malloc(sizeof(int)*size);
		while (min < max)
		{
			tab[i] = min;
			min++;
		}
		return (tab);
	}
	else
		return (0);
}

int		main(int ac, char **av)
{
	if (ac != 3)
		printf("erreur nombre d'argument");
	else
		ft_range(atoi(av[1]), atoi(av[2]));
	return(0);
}
