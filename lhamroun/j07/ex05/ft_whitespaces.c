#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_count_char(char *str)
{
	int i;

	i = 0;
	while (str[i]) //on compte tous les char de la string
		i++;
	return (i);
}

char	**ft_whitespaces(char	*str)
{
	int i;
	int x;
	int y;
	int j;
	char **tab;

	i = 0;
	x = 0;
	tab = (char **)malloc(sizeof(char)*ft_count_char(str));
	while (str[i])
	{
		y = 0;
		while (str[i] < 33)
		{
			i++;
			if (str[i] == '\0')//s'il n'y a que des espaces
				return (tab);
		}
		j = i;
		while (str[j] > 32 && str[j] < 127)//des qu'il y a un mot on l'alloue 
			j++;
		tab[x] = (char *)malloc(sizeof(char) * (j + 1));
		while (str[i] > 32 && str[i] < 127)//on pt colé ds tab
			tab[x][y++] = str[i++];
		x++;
	}
	return (tab);
}
