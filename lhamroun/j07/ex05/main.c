#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	ft_print_words_tables(char **tab);

char	**ft_whitespaces(char *str);

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("erreur nombre d'argument");
	else
		ft_print_words_tables(ft_whitespaces(av[1]));
	return(0);
}
