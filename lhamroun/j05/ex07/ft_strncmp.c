#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_strncmp(char *s1, char *s2, unsigned int n)
{
	int i;

	i = 0;
	while (s1[i] == s2[i] && i < n && (s1[i] || s2[i]))
		i++;
	return (s1[i] - s2[i]);
}

int		main(int ac, char **av)
{
	if (ac != 4)
		printf("erreur nombre d'argument");
	else
		printf("%d\n", ft_strncmp(av[1], av[2], atoi(av[3])));
	return(0);
}
