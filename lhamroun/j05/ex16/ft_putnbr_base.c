#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb, int x)
{
	int i;

	i = 0;
	if (nb < 0)
	{
		ft_putchar('-');
		nb = -nb;
	}
	else if (nb < x)
		ft_putchar(nb + '0');
	else
	{
		ft_putnbr(nb / x);
		ft_putnbr(nb % x);
	}
}

void	ft_msg_error()
{
	write(1, "Erreur\n", 1);
}

void	ft_putnbr_base(int nbr, char *base)
{
	int i;
	int x;

	i = 0;
	while (base[i])
	{
		if (base[i])
			
		i++;
	}
	if (i < 2)
		ft_msg_error();
	else
		x = --i;
}

int		main(int ac, char **av)
{
	if (ac != 3)
		printf("erreur nombre d'argument");
	else
		ft_putnbr_base(atoi(av[1]), av[2]);
	return(0);
}
