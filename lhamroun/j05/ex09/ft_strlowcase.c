#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	*ft_lowercase(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] >= 'A' && str[i] <='Z')
			str[i] = str[i] + 32;
		i++;
	}
	return (str);
}

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("erreur nombre d'argument");
	else
		printf("%s\n", ft_lowercase(av[1]));
	return(0);
}
