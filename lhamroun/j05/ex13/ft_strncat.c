#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	*ft_strncat(char *dest, char *src, int nb)
{
	int i;
	int j;

	i = 0;
	j = 0;;
	while(dest[i])
		i++;
	while (src[j] && j < nb)
	{
		dest[i] = src[j];
		i++;
		j++;
	}
	while (j < nb)
	{
		dest[i] = '\0';
		i++;
		j++;
	}
	dest[i] = '\0';
	return (dest);
}

int		main(int ac, char **av)
{
	if (ac != 4)
	printf("1e arg = dest, 2e arg = src, 3e arg = nb");
	else
	printf("%s\n", ft_strncat(av[1], av[2], atoi(av[3])));
	return(0);
}
