#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size) //dest + src[size]
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (src[j])
		j++;
	if (size == 0)
		return (j);
	while (dest[i] && i < size)
		i++;
	return (j + i);
}

int		main(int ac, char **av)
{
	if (ac != 4)
		printf("erreur nombre d'argument");
	else
		printf("%u\n", ft_strlcat(av[1],av[2], atol(av[3])));
	return(0);
}
