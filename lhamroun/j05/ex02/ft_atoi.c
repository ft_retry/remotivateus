#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_atoi(char *str)
{
	int i;
	int nb;
	int neg;

	i = 0;
	nb = 0;
	neg = 1;
	while (str[i] == '\n' || str[i] == '\t' || str[i] == '\v' || str[i] == '\f' || str[i] == '\r' || str[i] == ' ')
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			neg = -neg;
		i++;
	}
	if (str[i] < 48 || str[i] > 57)
		return (0);
	else
	{
		while (str[i] > '0' && str[i] < '9')
		{
			nb = nb * 10 + str[i] - '0';
			i++;
		}
	}
	return (nb * neg);
}

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("écris le bon nombre d'arguments");
	else
		printf("%d\n", ft_atoi(av[1]));
	return (0);
}
