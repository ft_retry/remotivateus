#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_str_is_alpha(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if ((str[i]<='a' || str[i]>='z') &&
		(str[i]<='A' || str[i]>='Z'))
			return (0);
		i++;
	}
	return (1);
}

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("erreur nombre d'argument");
	else
		printf("%d\n", ft_str_is_alpha(av[1]));
	return(0);
}
