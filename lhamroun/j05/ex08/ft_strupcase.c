#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	*ft_strupcase(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] >= 'a' && str[i] <= 'z')
			str[i] = str[i] - 32;
		i++;
	}
	return (str);
}

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("erreur sur le nombre d'argument");
	else
		printf("%s\n", ft_strupcase(av[1]));
	return(0);
}

