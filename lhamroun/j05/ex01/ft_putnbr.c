#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	int i;

	i = 0;
	if (nb < 0)
		nb = -nb;
	else if (nb < 10)
		ft_putchar(nb + '0');
	else
	{
		ft_putnbr(nb / 10);
		ft_putnbr(nb % 10);
	}
}

int		main(int ac, char **av)
{
	int n;

	if (ac != 2)
		return (0);
	else
		n = atoi(av[1]);
		ft_putnbr(n);
	return(0);
}
