#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	int i;

	i = 0;
	while (i < n)
	{
		if (src[i])
			dest[i] = src[i];
		else
			dest[i] = '\0';
		i++;
	}
	printf("%d\n", i);
	dest[i] = '\0';
	return (dest);
}

int		main(int ac, char **av)
{
	if (ac != 3)
		printf("1er arg = src et 2e arg = n");
	else
	{
		printf("%s\n", ft_strncpy(av[0], av[1], atoi(av[2])));
		return(0);
	}
}
