#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_str_is_numeric(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] <= '0' || str[i] >= '9')
			return (0);
		i++;
	}
	return (1);
}

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("erreur nombre d'argument");
	else
		printf("%d\n", ft_str_is_numeric(av[1]));
	return(0);
}
