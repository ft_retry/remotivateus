#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	**sudoku(char **sdku)
{
	int i;
	int j;
	int n;
	int a;

	i = 0;
	j = 0;
	n = 1;
	a = 0;
	while (i < 9)
	{
		if (!(sdku[i]=(char*)malloc(sizeof(char) * 10)))
			return (NULL);
		i++;
	}
	while (j < 9 && i < 9)
	{
		while (j < 9)
		{
			if (sdku[i][j] == '.')
			{
				while (a < j)
				{
					if (sdku[i][a] == n)
						n++;
					j = 0;
					a++;
				}
			}
			if (sdku[i][j] == n)
				n++;
			j++;
		}
		i++;
		j = 0;
	}
	return (sdku);
}

int		main(int ac, char **av)
{
	int i;
	int j;

	i = 1;
	while (i <= 9)
	{
		j = 0;
		while (av[i][j])
			j++;
		if (ac != 10 || j != 9)
		{
			write(1, "Erreur\n", 7);
			return (0);
		}
		i++;
	}
	if (ac == 10)
		sudoku(av);
	return(0);
}
