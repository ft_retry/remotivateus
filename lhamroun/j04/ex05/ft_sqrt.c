#include <stdio.h>

int		ft_sqrt(int nb)
{
	int i;

	i = nb / 2;
	if (nb < 1)
		return (0);
	else
	{
		while (i > 1)
		{
			if ((i * i) == nb)
				return (i);
			i--;
		}
		return (0);
	}
}

int		main()
{
	int n;

	n = 144;
	printf("%d\n", ft_sqrt(n));
	return(0);
}
