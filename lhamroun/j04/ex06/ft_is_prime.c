#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int		ft_is_prime(int nb)
{
	int i;

	i = 2;
	if (nb < 2)
		return (0);
	else
	{
		while (i <= nb / 2)
		{
			if (nb % i == 0)
				return (0);
			i++;
		}
		return (1);
	}
}

int		main()
{
	int n;

	n = 1;
	printf("%d\n", ft_is_prime(n));
	return(0);
}
