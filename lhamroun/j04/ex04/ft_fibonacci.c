#include <stdio.h>

int		ft_fibonacci(int index)
{
	int nb;

	if (index < 0)
		return (-1);
	if (index == 0)
		return (0);
	if (index == 1)
		return (1);
	else
	{
		nb = ft_fibonacci(index - 1) + ft_fibonacci(index - 2);
		return (nb);
	}
}

int		main()
{
	int i;

	i = 4;
	printf("%d\n", ft_fibonacci(i));
	return (0);
}
