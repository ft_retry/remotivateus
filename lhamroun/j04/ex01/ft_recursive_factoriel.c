#include <stdio.h>

int		ft_recursive_factoriel(int nb)
{
	int n;

	n = 1;
	if (nb < 0 || nb > 12)
		return (0);
	else if (nb == 0)
		return (1);
	else
		return (nb * ft_recursive_factoriel(--nb));
}

int main()
{
	int nb = 4;

	printf("\n\n%d\n\n", ft_recursive_factoriel(nb));
	return (0);
}
