#include <stdio.h>

int		ft_iterretive_power(int nb, int power)
{
	int i;

	i = nb;
	if (power < 0)
		return (0);
	else if (power == 0)
		return (1);
	else if (power == 1)
		return (nb);
	else
	{
		while (power > 1)
		{
			i = i * nb;
			power--;
		}
		return (i);
	}
}

int		main()
{
	int nb;
	int power;

	nb = 2;
	power = 7;
	printf("%d\n", ft_iterretive_power(nb, power));
	return (0);
}
