#include <unistd.h>

int		ft_iterrative_factorial(int nb)
{
	int n;

	n = 1;
	if (nb < 1 || nb > 12)
		return (0);
	else
	{
		while (nb >= 1)
		{
			n = n * nb;
			nb--;
		}
		return (n);
	}
}

int main(int argc, char **argv)
{
	int nb = 14;
	printf("%d\n", ft_iterrative_factorial(nb));
	return (0);
}

