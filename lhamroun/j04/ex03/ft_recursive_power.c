#include <stdio.h>

int		ft_recurssive_power(int nb, int power)
{
	int n;

	n = nb;
	if (power < 0)
		return (0);
	else if (power == 0)
		return (1);
	else if (power == 1)
		return (nb);
	else
	{
		n = n * ft_recurssive_power(nb, --power);
		return (n);
	}
}

int		main()
{
	int nb;
	int power;

	nb = 4;
	power = -2;
	printf("%d\n", ft_recurssive_power(nb, power));
	return (0);
}
