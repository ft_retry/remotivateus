#include <stdio.h>

int		ft_is_prime(int nb)
{
  int i;

		i = 2;
		if (nb < 2)
			return (0);
		else
		{
			while (i <= nb / 2)
			{
			if (nb % i == 0)
				return (0);
			i++;
			}
		}
		return (1);
}

int		ft_find_next_prime(int nb)
{
	while (ft_is_prime(nb) != 1)
		nb++;
	return (nb);
}

int		main()
{
	int nb;

	nb = 18;
	printf("%d\n", ft_find_next_prime(nb));
	return(0);
}
