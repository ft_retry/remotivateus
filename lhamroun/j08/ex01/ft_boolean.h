#ifndef FT_BOOLEAN
#define FT_BOOLEAN

#include <unistd.h>

#define FALSE 0
#define SUCCESS 0
#define TRUE 1
#define EVEN(nbr) (!((nbr) % 2))//pas besoin de mettre nbr, avec n'importe quel variable ca marche
#define EVEN_MSG "J'ai un nombre pair d'arguments.\n"
#define ODD_MSG  "J'ai un nombre impair d'arguments.\n"
typedef int t_bool;//define ne marche pas

#endif

/*
 * En règle général, vaut mieux utilisé typedef pour définir
 * un type, par convention, pour dire qu'un type provient 
 * d'un typedef, on ajoute "t_" avant le nom de notre type 
 * comme t_bool qu remplace int.
 * #define est un alias : #define dest source 
 * alors que typedef source dest; + ";"
 * */
