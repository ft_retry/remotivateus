#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_count_char(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0') //on compte tous les char de la string
		i++;
	return (i);
}

char	**ft_whitespaces(char	*str)
{
	int i;
	int x;
	int y;
	int j;
	char **tab;

	i = 0;
	x = 0;
	if (!(tab = (char **)malloc(sizeof(char)*ft_count_char(str))))
	return NULL;
	while (str[i] != '\0')
	{
		y = 0;
		while (str[i] < 33)
		{
			i++;
			if (str[i] == '\0')
				return (tab);
		}
		j = i;
		while (str[j] > 32 && str[j] < 127)
			j++;
		if (!(tab[x] = (char *)malloc(sizeof(char) * (j + 1))))
			return (NULL);
		while (str[i] > 32 && str[i] < 127)
			tab[x][y++] = str[i++];
		x++;
	}
	x = 0;
	y = 0;
	while (tab[x] != '\0')
	{
		y = 0;
		while (tab[x][y] != '\0')
		{
			printf("%c",tab[x][y++]);
		}
		printf("\n");
		x++;
	}
	return (tab);
}

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("erreur nombre d'argument");
	else
		ft_whitespaces(av[1]);
	return(0);
}
