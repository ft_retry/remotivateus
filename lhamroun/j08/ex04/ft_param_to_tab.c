#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ft_stock_par.h"

char	**ft_split_whitespaces(char *str);

//struct S_stock_par = char *

char	*ft_strcpy(char *dest, char *src)
{
	int i;

	i = 0;
	while (src[i])
		dest[i++] = src[i++];
	dest[i] = '\0';
	return (dest);
}

struct s_stock_par *ft_param_to_tab(int ac, char **av)
{
	t_stock_par tab;
	int i;
	int j;

	i = 0;
	while (av[i])//while pour malloc tous les arguments
	{
		j = 0;
		while (av[i][j])
			j++;
		if (!(av[i] = (char *)malloc(sizeof(char)*j)))
			return (NULL);
		i++;
	}
	i = 0;
	while (av[i])
		tab->words[i++] = ft_strcpy(tab->words[i++], av[i++]);
	return (&tab);
}
