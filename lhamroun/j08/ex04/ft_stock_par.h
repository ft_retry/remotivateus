#ifndef FT_STOCK_PAR
#define FT_STOCK_PAR

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct s_stock_par
{
	int size_param;//longueur paramètre
	char *copy;//adresse parametre
	char **tab;//tableau retourné par WS
}				t_stock_par;

t_stock_par *ft_param_to_tab(int ac, char **av);
char **ft_split_whitespaces(char *str);

#endif
