#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ft_abs.h"

int		ft_absolu(int x)
{
	return (ABS(x));
}

int		main(int ac, char **av)
{
	if (ac != 2)
		printf("erreur nombre d'argument");
	else
		printf("%d\n", ft_absolu(atoi(av[1])));
	return(0);
}
