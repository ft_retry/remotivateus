#include <unistd.h>
#include <stdio.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	long n;

	n = nb;
	if (n < 0)
	{
		ft_putchar('-');
		n = -n;
	}
	if (n > 9)
	{
		//printf("%ld\n", n);
		//ft_putchar('\n');
		ft_putnbr(n / 10);
		printf("%ld\n", n);
		//ft_putnbr(n % 10);
		ft_putchar('\n');
		//printf("%ld\n", n);
	}
	else
	{
		ft_putchar(nb + '0');
		ft_putchar('\n');
	}
		
}

int	main()
{
	ft_putnbr(12345);
	return (0);
}
