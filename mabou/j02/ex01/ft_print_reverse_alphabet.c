#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_print_reverse_alphabet(void)
{
	char a;

	a = 122;
	while (a >= 97)
	{
		ft_putchar(a);
		a--;
	}
}

int	main()
{
	ft_print_reverse_alphabet();
	return (0);
}
