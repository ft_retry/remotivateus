#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	long n;

	n = nb;
	if (n < 0)
	{
		ft_putchar('-');
		n = -n;
	}
	if (n > 9)
	{
		ft_putnbr(n / 10);
		ft_putnbr(n % 10);
	}
	else
		ft_putchar(nb + '0');
}

void	ft_increment(int a, int b)
{
	if (a < b)
	{
		ft_putnbr(a);
		ft_putchar(' ');
		ft_putnbr(b);
		if (!(a == 98 && b == 99))
		{
			ft_putchar(',');
			ft_putchar(' ');
		}
	}
}

void	ft_print_comb2(void)
{
	int a;
	int b;

	a = 0;
	while (a <= 98)
	{
		b = 0;
		while (b <= 99)
		{
		       	ft_increment(a, b);
			b++;
		}
		a++;
	}
}

int	main()
{
	ft_print_comb2();
	return (0);
}
